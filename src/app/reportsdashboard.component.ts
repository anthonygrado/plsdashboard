import { Component } from '@angular/core';

@Component({
	moduleId: module.id
  ,selector: 'reports-dashboard'  
  ,templateUrl: './reportsdashboard.component.html'  
})

export class ReportsDashboard {
	title = 'Reporting Dashboard';
}