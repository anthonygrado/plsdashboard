import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CourseCustomizer } from './coursecustomizer/coursecustomizer.component';
import { ReportsDashboard } from './reportsdashboard.component';
import { LearnerHistoryReport } from './reporting/learnerhistory/learnerhistoryreport.component';
import { CEResultsReport } from './reporting/ceresults/ceresultsreport.component';


const routes: Routes = [
  { path: '', redirectTo: '/', pathMatch: 'full' }
  ,{ path: 'reports', component: ReportsDashboard }
  ,{ path: 'coursecustomizer', component: CourseCustomizer}
  ,{ path: 'reports/ceresults', component: CEResultsReport }
  ,{ path: 'reports/learnerHistory', component: LearnerHistoryReport }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ]
  ,exports: [ RouterModule ]
})

export class AppRoutingModule {}