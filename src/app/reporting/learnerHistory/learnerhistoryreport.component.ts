import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpModule, JsonpModule, Response } from '@angular/http';

import { ajax } from 'rxjs/observable/dom/ajax';
import { AjaxResponse } from 'rxjs/observable/dom/AjaxObservable.js';
import { Observable } from "rxjs/Rx";
import { User } from '../shared/user';

@Component({
	moduleId: module.id
  ,selector: 'learnerhistoryreport'
  ,templateUrl: './learnerhistoryreport.component.html'  
 })

export class LearnerHistoryReport {
	title = "Learner History Report"

    public user: User;

	public orgs = [
    {id:'0', value:'', name:"User's Organization"},
    {id:'1', value:'5809A84E-01FF-4443-AF66-FAE22D639BE7', name:"Akron Children's Hospital"},
    {id:'2', value:'C7D7CAD7-F3E5-42E4-8AF8-66149367E42C', name:"All Children's Hospital Johns Hopkins Medicine"},
    {id:'3', value:'D79E1810-3213-4612-8FDB-19D19F8419F4', name:"Ann & Robert H. Lurie Children's Hospital of Chicago"},
    {id:'4', value:'888D0E52-3AFA-42CE-81A5-99B8D2AA8DF3', name:"Arkansas Children's Hospital"},
    {id:'5', value:'3343C625-072A-438F-966D-8653E9AA7317', name:"Children's Health Children's Medical Center Dallas"},
    {id:'6', value:'42A7AF0D-DD0B-490E-9C42-C30C707836BF', name:"Children's Hospital & Medical Center"},
    {id:'7', value:'E62E11C0-6FFB-4FA9-8B21-F422959940AF', name:"Children's Hospital Association"},
    {id:'8', value:'888D8B0B-F937-4F22-81CF-2BB09C26B261', name:"Children's Hospital Colorado"},
    {id:'9', value:'5495011B-5BA1-40F5-95FB-6EE8B00FC3A5', name:"Children's Hospital Los Angeles"},
    {id:'10', value:'3506823E-6667-4E61-8408-863DE918229D', name:"Children's Hospital of Pittsburgh of UPMC"},
    {id:'11', value:'166D279F-F652-473E-8E8F-0571C3746A2F', name:"Children's Hospital of Wisconsin"},
    {id:'12', value:'F7C7EABB-1309-4509-804F-5188C5865025', name:"Children's Hospitals and Clinics of Minnesota"},
    {id:'13', value:'16480B00-3D05-4A46-98AB-AD903ACDC1E5', name:"Children's Mercy Kansas City"},
    {id:'14', value:'F8424027-E0BC-4B50-B9A4-11EB4C448C76', name:"Children's National Medical Center"},
    {id:'15', value:'FBCBEE87-D5E1-4350-9475-872C648E356C', name:"Children's of Alabama"},
    {id:'16', value:'A3ED6724-C121-4F09-BA3A-299C51E47A4B', name:"CHOC Children's Hospital of Orange County"},
    {id:'17', value:'FF75FA49-AA64-47CC-A4D2-4C3612F740D2', name:"Cincinnati Children's Hospital Medical Center"},
    {id:'18', value:'44AB611F-E909-490A-9D66-31BB92B8707A', name:"Connecticut Children's Medical Center"},
    {id:'19', value:'DE1EAB56-E5F0-46BE-AD8F-600E85FAF9DD', name:"Cook Children's Medical Center"},
    {id:'20', value:'968CD660-BE02-4EC6-ABE6-19980FF489C3', name:"Driscoll Children's Hospital"},
    {id:'21', value:'0044541E-EBE3-4D91-929F-7A2AF3CCB810', name:"Le Bonheur Children's Hospital"},
    {id:'22', value:'C84D529C-A47E-4C39-9433-A08B32464119', name:"Loma Linda University Children's Hospital"},
    {id:'23', value:'D3B5C4EC-C302-4069-A52B-F753CFBFC521', name:"Lucile Packard Children's Hospital Stanford"},
    {id:'24', value:'262CBAD0-A89B-4F2E-8F41-13DB4DBA9758', name:"Mercy Health System"},
    {id:'25', value:'91531367-6F76-47FC-B1B8-AABE58C31A00', name:"Nationwide Children's Hospital"},
    {id:'26', value:'40F0AC5D-69B0-4661-B04E-037CA718ABCD', name:"Nicklaus Children's Hospital, formerly Miami Children's Hospital"},
    {id:'27', value:'DEBF4F96-D3CF-4585-8219-A488096FD32D', name:"Phoenix Children's Hospital"},
    {id:'28', value:'50402A02-115D-434F-9193-0C1D8B3A20DA', name:"Rady Children's Hospital San Diego"},
    {id:'29', value:'7235D673-3956-4136-AD85-49BCFF64C904', name:"Seattle Children's"},
    {id:'30', value:'211614C6-EB55-4BC0-8E96-9C96F3B5EE36', name:"St. Louis Children's Hospital"},
    {id:'31', value:'25A0FA74-34A0-489E-8CF9-568273B9C46A', name:"Swedish Medical Center"},
    {id:'32', value:'306E37B2-58EF-4E2E-B1B3-C9F4D30388F6', name:"Test Hospital"},
    {id:'33', value:'8E30AE5B-9585-4D76-8076-1D4BAEEA61EF', name:"Texas Children's Hospital"},
    {id:'34', value:'4F45DF3B-FF05-43AB-91C7-E3D5059EB54E', name:"The Children's Hospital of Philadelphia"},
    {id:'35', value:'40C06046-AD98-421E-8331-87360BED020F', name:"Valley Children's Hospital"},
    {id:'36', value:'490EB51E-F628-4A18-AC08-D70DB02D29AC', name:"American Association of Critical Care Nurses (AACN)"},
    {id:'37', value:'E62E11C0-6FFB-4FA9-8B21-F422959940AF', name:"Children's Hospital Association"},
    {id:'38', value:'78A875C0-DBCF-4F46-86F0-39969D0E54E6', name:"HealthcareSource"}
  ];

  ngOnInit() {
    this.user = {
      firstName: ''
      ,lastName: ''
      ,userOrg: this.orgs[0].value
    }
  }    
}