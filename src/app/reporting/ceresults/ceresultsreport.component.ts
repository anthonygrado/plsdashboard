import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpModule, JsonpModule, Response } from '@angular/http';

import { ajax } from 'rxjs/observable/dom/ajax';
import { AjaxResponse } from 'rxjs/observable/dom/AjaxObservable.js'
import { Observable } from "rxjs/Rx";

import { CertificateType } from '../shared/certificatetype'

@Component({
	moduleId: module.id
  ,selector: 'ceresultsreport'
  ,templateUrl: './ceresultsreport.component.html'  
 })

export class CEResultsReport {
	title = "CE Results Report"    

  public certificate: CertificateType;

	public certificates = [
    {id:'1', value:'', name:"ALL"}
    ,{id:'2', value:'cme', name:"CME"}
    ,{id:'3', value:'cne', name:"CNE"}
    ,{id:'4', value:'crce', name:"CRCE"}
    
  ];

  ngOnInit() {
    this.certificate = {
      certificatelabel: ''
      ,certificateid: this.certificates[0].value

    }
  }  

  submitSearch(f: CertificateType) { }
}