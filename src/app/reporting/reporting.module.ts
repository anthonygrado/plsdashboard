import '../rxjs-extensions';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from '../app-routing.module';
import { LearnerHistoryReport } from './learnerhistory/learnerhistoryreport.component';
import { CEResultsReport } from './ceresults/ceresultsreport.component';

@NgModule({
  declarations: [   
    CEResultsReport
    ,LearnerHistoryReport    
  ],
  imports: [
    AppRoutingModule
    ,BrowserModule
    ,FormsModule
    ,HttpModule   
  ],
  providers: []  
})
export class ReportingModule { }
