import './rxjs-extensions';
import 'hammerjs';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CourseCustomizer } from './coursecustomizer/coursecustomizer.component';
import { ReportingModule } from './reporting/reporting.module';
import { ReportsDashboard } from './reportsdashboard.component';

@NgModule({
  declarations: [
    AppComponent
    ,ReportsDashboard
    ,CourseCustomizer
  ],
  imports: [
    BrowserModule
    ,FormsModule
    ,HttpModule
    ,NgbModule.forRoot()
    ,AppRoutingModule
    ,ReportingModule
  ],
  providers: [ ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
